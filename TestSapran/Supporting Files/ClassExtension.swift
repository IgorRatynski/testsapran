//
//  ClassExtension.swift
//  TestSapran
//
//  Created by Igor Ratynski on 30.08.2018.
//  Copyright © 2018 Igor Ratynski. All rights reserved.
//

import Foundation

extension Bundle {
    
    func apiBaseUrl() -> String {
        return object(forInfoDictionaryKey: "ApiBaseUrl") as? String ?? ""
    }
    
}
