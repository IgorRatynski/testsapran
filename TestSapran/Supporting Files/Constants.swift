//
//  Constants.swift
//  TestSapran
//
//  Created by Igor Ratynski on 29.08.2018.
//  Copyright © 2018 Igor Ratynski. All rights reserved.
//

import Foundation

let kMain = "Main"

let kEmployeeDataUpdatedNotification = NSNotification.Name("kEmployeeDataUpdatedNotification")
