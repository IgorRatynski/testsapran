//
//  ModelProtocols.swift
//  TestSapran
//
//  Created by Igor Ratynski on 29.08.2018.
//  Copyright © 2018 Igor Ratynski. All rights reserved.
//

protocol EmployeeListModelProtocol {
    var employeeList: [Employe] { get }
}
