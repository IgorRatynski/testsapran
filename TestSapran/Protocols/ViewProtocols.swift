//
//  ViewProtocols.swift
//  TestSapran
//
//  Created by Igor Ratynski on 29.08.2018.
//  Copyright © 2018 Igor Ratynski. All rights reserved.
//

import UIKit

protocol EmployeeListViewProtocol: class {
    var tableView: UITableView! { get }
}

protocol EmployeDetailsViewProtocol: class {
    var nameLabel: UILabel! { get }
    var addressLabel: UILabel! { get }
    var phoneLabel: UILabel! { get }
    var positionLabel: UILabel! { get }
}

protocol EmployeTableViewCellProtocol {
    var positionNumberLabel: UILabel! { get }
    var nameLabel: UILabel! { get }
}
