//
//  PresenterProtocols.swift
//  TestSapran
//
//  Created by Igor Ratynski on 29.08.2018.
//  Copyright © 2018 Igor Ratynski. All rights reserved.
//

import Foundation

protocol Updatable: class {
    func dataUpdated()
}

protocol EmployeeListPresenterProtocol: Updatable {
    func attach(view: EmployeeListViewProtocol)
    
    func setup(cell: EmployeTableViewCellProtocol, indexPath: IndexPath)
    func employeeCount() -> Int
    func employeSelectedAt(indexPath: IndexPath)
}
