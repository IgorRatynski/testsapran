//
//  Employe.swift
//  TestSapran
//
//  Created by Igor Ratynski on 29.08.2018.
//  Copyright © 2018 Igor Ratynski. All rights reserved.
//

import Foundation
import Fakery

struct Employe: Decodable {
    
    let name: String
    let address: String
    let phone: String
    let position: String
    
    static var fakeDataSet: [Employe] {
        let faker = Faker()
        var result: [Employe] = []
        var address = ""
        
        for _ in 0...20 {
            address = faker.address.secondaryAddress() + " " + faker.address.streetName() + ", " + faker.address.city() + ", " + faker.address.state()
            result.append(Employe(name: faker.name.name(), address: address, phone: faker.phoneNumber.phoneNumber(), position: faker.company.bs()))
        }
        
        return result
    }
}
