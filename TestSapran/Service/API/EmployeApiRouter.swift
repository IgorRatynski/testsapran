//
//  EmployeApiRouter.swift
//  TestSapran
//
//  Created by Igor Ratynski on 29.08.2018.
//  Copyright © 2018 Igor Ratynski. All rights reserved.
//

import Alamofire
import Foundation

enum EmployeApiRouter: URLRequestConvertible {
    
    // MARK: - Router
    
    case get()
    
    // MARK: - URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: ApiService.Params.baseUrl.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .get: urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
        }
        
        return urlRequest
    }
    
    // MARK: - Private
    
    private var method: HTTPMethod {
        switch self {
        case .get: return .get
        }
    }
    
    private var path: String {
        switch self {
        case .get: return "/get/bOYurmMDEy" // check!
        }
    }
    
}
