//
//  EmployeApiService.swift
//  TestSapran
//
//  Created by Igor Ratynski on 29.08.2018.
//  Copyright © 2018 Igor Ratynski. All rights reserved.
//

import Alamofire
import Foundation

class EmployeApiService: ApiService {
    
    // MARK: - Public
    
    func get(success: @escaping (Data) -> Void,
             failure: @escaping (ServiceFailureType) -> Void) {
        
        _ = self.sessionManager.request(EmployeApiRouter.get())
            .validate(statusCode: [200])
            .responseJSON { response in
                guard let data = response.data else {
                    failure(.connection)
                    return
                }
                
                if let error = response.error {
                    if error as? AFError == nil {
                        failure(.connection)
                    } else {
                        failure(.server)
                    }
                    return
                }
                
                success(data)
        }
    }
    
}
