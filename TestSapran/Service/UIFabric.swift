//
//  UIFabric.swift
//  TestSapran
//
//  Created by Igor Ratynski on 29.08.2018.
//  Copyright © 2018 Igor Ratynski. All rights reserved.
//

import UIKit

class UIFabric {
    
    enum Employe: String {
        case eployeDetails = "Eploye Details"
    }
    
    static func screen(type: Employe) -> UIViewController {
        return UIStoryboard(name: kMain, bundle: nil).instantiateViewController(withIdentifier: type.rawValue)
    }
}

extension UIFabric {

    static func label() -> UILabel {
        let label = UILabel()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }
}
