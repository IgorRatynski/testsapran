//
//  DataService.swift
//  TestSapran
//
//  Created by Igor Ratynski on 29.08.2018.
//  Copyright © 2018 Igor Ratynski. All rights reserved.
//

import Foundation

class DataService {
    
    static let shared = DataService()
    private init() { }
    
    public var selectedEmploye: Employe?
    
    private let apiService = EmployeApiService()
    private(set) var employee: [Employe] = [] {
        didSet { NotificationCenter.default.post(name: kEmployeeDataUpdatedNotification, object: nil) } }
    
    // MARK: - Public
    
    func get() {
        apiService.get(success: { (data) in
            self.success(data: data)
        }, failure: { failure in
            self.failure(failure)
        })
    }
    
    // MARK: - Private
    
    private func success(data: Data) {
        DispatchQueue.main.async {
            if let employe = self.jsonDecode(data: data) {
                self.employee = employe
            } else {
                self.employee = Employe.fakeDataSet
            }
        }
    }
    
    private func failure(_ failure: ServiceFailureType) {
        DispatchQueue.main.async {
            self.employee = Employe.fakeDataSet
        }
    }
    
    func jsonDecode(data: Data) -> [Employe]? {
        do {
            return try JSONDecoder().decode([Employe].self, from: data)
        } catch {
            return nil
        }
    }
}

enum ServiceFailureType {
    case connection
    case server
}

