//
//  Router.swift
//  TestSapran
//
//  Created by Igor Ratynski on 29.08.2018.
//  Copyright © 2018 Igor Ratynski. All rights reserved.
//

import UIKit

extension UIViewController {
    
    enum Routing {
        case eployeDetails
    }
    
    enum ViewType {
        case undefined
        
        case eployeeList
    }
    
    private struct Keys {
        static var key = "\(#file)+\(#line)"
    }
    
    var type: ViewType {
        get { return objc_getAssociatedObject(self, &Keys.key) as? ViewType ?? .undefined }
        set { objc_setAssociatedObject(self, &Keys.key, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }
    
    func routing(with routing: Routing) {
        switch type {
            
        case .eployeeList: eployeeList(with: routing)
            
        default:break
        }
    }
}

private extension UIViewController {
    
    func eployeeList(with routing: Routing) {
        switch routing {
            
        case .eployeDetails: navigationController?.pushViewController(UIFabric.screen(type: .eployeDetails), animated: true)
        }
    }
    
}
