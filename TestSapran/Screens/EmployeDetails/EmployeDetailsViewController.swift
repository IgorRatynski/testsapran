//
//  EmployeDetailsViewController.swift
//  TestSapran
//
//  Created by Igor Ratynski on 29.08.2018.
//  Copyright © 2018 Igor Ratynski. All rights reserved.
//

import UIKit

class EmployeDetailsViewController: UIViewController, EmployeDetailsViewProtocol {
    
    var nameLabel: UILabel!
    var addressLabel: UILabel!
    var phoneLabel: UILabel!
    var positionLabel: UILabel!
    
    private let presenter = EmployeDetailsPresenter()

    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    // MARK: - Private
    
    private func setup() {
        initLabels()
        presenter.attach(view: self)
    }
    
    private func initLabels() {
        addLabels()
        addLabelsToView()
        addConstraints()
    }
    
    private func addLabels() {
        nameLabel = UIFabric.label()
        addressLabel = UIFabric.label()
        phoneLabel = UIFabric.label()
        positionLabel = UIFabric.label()
    }
    
    private func addLabelsToView() {
        view.addSubview(nameLabel)
        view.addSubview(addressLabel)
        view.addSubview(phoneLabel)
        view.addSubview(positionLabel)
    }
    
    private func addConstraints() {
        
        addConstraintsTo(element: nameLabel, topElement: view, bottomElement: addressLabel, isFirst: true)
        addConstraintsTo(element: addressLabel, topElement: nameLabel, bottomElement: phoneLabel)
        addConstraintsTo(element: phoneLabel, topElement: addressLabel, bottomElement: positionLabel)
        addConstraintsTo(element: positionLabel, topElement: phoneLabel, bottomElement: view, isLast: true)
    }
    
    private func addConstraintsTo(element: UIView, topElement: UIView, bottomElement: UIView, isFirst:Bool = false, isLast: Bool = false) {
        
        if isFirst {
            element.topAnchor.constraint(equalTo: view.topAnchor, constant: 8).isActive = true
        } else {
            element.topAnchor.constraint(equalTo: topElement.bottomAnchor, constant: 8).isActive = true
        }
        
        element.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        element.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        
        if isLast {
            element.bottomAnchor.constraint(lessThanOrEqualTo: bottomElement.bottomAnchor, constant: -8).isActive = true
        } else {
            element.bottomAnchor.constraint(equalTo: bottomElement.topAnchor, constant: -8).isActive = true
        }
        
    }
}
