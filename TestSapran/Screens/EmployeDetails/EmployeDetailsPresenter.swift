//
//  EmployeDetailsPresenter.swift
//  TestSapran
//
//  Created by Igor Ratynski on 29.08.2018.
//  Copyright © 2018 Igor Ratynski. All rights reserved.
//

import Foundation


class EmployeDetailsPresenter {
    
    private weak var view: EmployeDetailsViewProtocol?
    
    // MARK: - Public
    
    public func attach(view: EmployeDetailsViewProtocol) {
        self.view = view
        setup()
    }
    
    // MARK: - Private
    
    private func setup() {
        view?.nameLabel.text = "Имя: " + DataService.shared.selectedEmploye!.name
        view?.addressLabel.text = "Адрес: " + DataService.shared.selectedEmploye!.address
        view?.phoneLabel.text = "Телефон: " + DataService.shared.selectedEmploye!.phone
        view?.positionLabel.text = "Должность: " + DataService.shared.selectedEmploye!.position
    }
}
