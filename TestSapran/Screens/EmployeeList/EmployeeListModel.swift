//
//  EmployeeListModel.swift
//  TestSapran
//
//  Created by Igor Ratynski on 29.08.2018.
//  Copyright © 2018 Igor Ratynski. All rights reserved.
//

import Foundation


class EmployeeListModel: EmployeeListModelProtocol {
    
    var employeeList: [Employe] = []
    weak var presenter: Updatable?
    
    public func setup(presenter: Updatable) {
        self.presenter = presenter
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateData), name: kEmployeeDataUpdatedNotification, object: nil)
    }
    
    @objc private func updateData() {
        employeeList = DataService.shared.employee
        presenter?.dataUpdated()
    }
    
}
