//
//  EmployeTableViewCell.swift
//  TestSapran
//
//  Created by Igor Ratynski on 29.08.2018.
//  Copyright © 2018 Igor Ratynski. All rights reserved.
//

import UIKit


class EmployeTableViewCell: UITableViewCell, EmployeTableViewCellProtocol {
    
    typealias PositionNumber = String
    static let identifier = "EmployeTableViewCell"
    
    @IBOutlet weak var positionNumberLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Public
    
    public func setup(position: PositionNumber, name: String) {
        positionNumberLabel.text = position
        nameLabel.text = name
    }
}

