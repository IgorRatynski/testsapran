//
//  EmployeeListViewController.swift
//  TestSapran
//
//  Created by Igor Ratynski on 29.08.2018.
//  Copyright © 2018 Igor Ratynski. All rights reserved.
//

import UIKit

class EmployeeListViewController: UIViewController, EmployeeListViewProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    
    private let presenter: EmployeeListPresenterProtocol = EmployeeListPresenter()
    
    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    // MARK: - Private
    
    private func setup() {
        type = .eployeeList
        presenter.attach(view: self)
    }
}

extension EmployeeListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        presenter.employeSelectedAt(indexPath: indexPath)
        routing(with: .eployeDetails)
    }
}

extension EmployeeListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.employeeCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: EmployeTableViewCell.identifier, for: indexPath) as! EmployeTableViewCell
        presenter.setup(cell: cell, indexPath: indexPath)
        
        return cell
    }
}
