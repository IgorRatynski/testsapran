//
//  EmployeeListPresenter.swift
//  TestSapran
//
//  Created by Igor Ratynski on 29.08.2018.
//  Copyright © 2018 Igor Ratynski. All rights reserved.
//

import UIKit

class EmployeeListPresenter: EmployeeListPresenterProtocol {
    
    private weak var view: EmployeeListViewProtocol?
    private let model = EmployeeListModel()
    
    // MARK: - Public
    
    public func attach(view: EmployeeListViewProtocol) {
        self.view = view
        setup()
    }
    
    // MARK: - EmployeeListPresenter
    
    public func setup(cell: EmployeTableViewCellProtocol, indexPath: IndexPath) {
        let cellModel = model.employeeList[indexPath.row]
        
        cell.positionNumberLabel.text = String(indexPath.row + 1)
        cell.nameLabel.text = cellModel.name
    }
    
    public func employeeCount() -> Int {
        return model.employeeList.count
    }
    
    public func employeSelectedAt(indexPath: IndexPath) {
        DataService.shared.selectedEmploye = model.employeeList[indexPath.row]
    }
    
    // MARK: - Updatable
    
    public func dataUpdated() {
        view?.tableView.reloadData()
    }
    
    // MARK: Private
    
    private func setup() {
        DataService.shared.get()
        model.setup(presenter: self)
    }
}


